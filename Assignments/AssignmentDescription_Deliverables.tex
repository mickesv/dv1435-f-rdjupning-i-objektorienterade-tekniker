\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[utf8]{inputenc}
%\usepackage[swedish]{babel}


\title{DV1435 Assignment Description}
\author{Mikael Svahnberg\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
Mikael.Svahnberg@bth.se}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\begin{abstract}
This document describes the assignments and the assignment deliveries in the course DV1435 ``Fördjupning i Objektorienterade Tekniker'' at Blekinge Institute of Technology.
\end{abstract}

\section{Assignment Submission}
All reports have to be submitted through Its Learning at specific deadlines. Timely delivery, appropriate contents and format, readability, and comprehensibility are the key factors evaluated for each document and contribute to your and your team's grade, aling with the criteria in the assignment rubrics (see separate document).

\vspace{0.5cm}
\noindent{\bf Frontpage} Each deliverable should have a frontpage that clearly shows at least the following information:

\begin{itemize}
\item course name
\item name and date of the deliverable
\item team name
\item names and e-mail addresses of all team members
\end{itemize}

\section{TaPE: Team and Peer Evaluations}
For the three-week rollcall, we use Team and Peer Evaluations. This allows us to catch any potential group problems early so that we can do something about them in the course. The team and peer evaluations are available in a google form, linked from Its learning. Submit one TaPE for each team member, including yourself.

\section{R: Progress Reports}
Progress reports are important to facilitate project tracking and control and to keep project stakeholders informed on the status and progress of a project. To be useful, a progress report should summarize key project data in an easy to read way. Depending on the type of project and type of development process, this key data can vary significantly.

A progress report for DV1435 should contain at least the following information:

\begin{itemize}
\item A short summary of the work done in the current week;
\item A list of major design decisions made since the last report. For each decision you should give a rationale for the decision;
\item A brief description of the changes to the WBS (work breakdown structure);
\item A list of major issues, problems, or risks together with basic information on their status;
\item A short summary of the work planned for the coming week; and
\item A summary of the current project status in terms of:
\begin{itemize}
\item schedule compliance, i.e. how well the project follows the current schedule;
\item resources spent (per person and week); and
\item product metrics, like the size or quality of work products.
\item Cumulative Planned Value, Cumulative Actual Cost, and Cumulative Earned Value.
\end{itemize}
\end{itemize}

The current project status should be easy to follow up week by week. It is therefore strongly recommended to present the data in tables and/or graphs that contain week-by-week data and totals. If planned in a good way, you can easily reuse this part of the progress report and just update it with the data from the current week. It might be worth some up-front effort to define a good presentation style, since a summary will be also needed in the final report (L4).

The actual delivery of the weekly report is done through a google form, linked from Its learning. In this form, some items are filled in directly, and others through pointing to a document.

It is strongly recommended that you maintain the work breakdown structure (WBS) either in a spreadsheet or in an agile project tracking tool, e.g. a kanban tool. For each item in the WBS, please report the following:

\begin{itemize}
\item Item name
\item estimated total effort for the item,  $t$
\item estimated current progress on the item, $p, 0<p<1$
\item spent time on the item, $s$
\item projected time for the item $s/p$
\end{itemize}

With this data you can then extrapolate for the entire project whether you are on, before, or after schedule. Please note that ALL items should be in the WBS, such as meetings, preparing weekly reports, and attending lectures. Your weekly velocity ought to be planned to roughly 20h per person.

\section{L1: High-Level Design}
This document describes the architecture (the ``big picture'') of the software system, i.e. its basic components (packages) and their interactions. The document should contain one or more architecture diagrams that show how the components of the proposed system fit within the framework as described in the textbook. For each major component, the document should briefly describe its purpose/functionality.

In addition to the system’s architecture as described above L1 should also briefly describe the development environment and target environment(s) for the system under development and the project's WBS (work breakdown structure). There should be a clear mapping between the items in the WBS and the system's architecture.

\section{L2: Detailed Design}
This document describes the software system on a more detailed level. It should start with a brief overview over the current overall architecture (i.e. an updated summary of L1). For the relevant parts of the architecture, i.e. those parts that your team has developed, changed or reused, L2 should present UML class diagrams. Other diagrams, like UML package or state machine diagrams can be added, if necessary.

Each component/class in L2 should be explained at a sufficient level of detail, so that someone else could implement/change/reuse the component/class in the way intended by the system’s designers.

\section{L3: Prototype}
This deliverable should ``prove'' that you have made significant progress in the project. This should be shown by at least the following two items:

\begin{itemize}
\item One or more (documented) UML state machine diagrams describing the key functionality of the system.
\item A prototype demo that shows at least the following functionality:
\begin{itemize}
\item Pacman moves straight and around a corner;
\item Pacman finds and eats food; and
\item Pacman is taken by a ghost.
\end{itemize}

\end{itemize}

Please record the demo in some way (Video) that can be viewed on either a PC or a Mac. We prefer if you upload your recording somewhere and provide the link in L3. However, you can also submit your recording through Its Learning.
Please note that the purpose of L3 is not to describe the GUI or all operations a user of the system has at his or her disposal. The purpose is to get a concise description of how your design “works”.

\section{L4: Final report}
The final report basically consists of updated summaries of L1 – L3, the final project status, plus a post mortem analysis of the project.

The final report should contain at least the following information:

\begin{itemize}
\item An overview over the system, including one or more diagrams of the system’s high-level architecture (i.e., an updated summary of L1).
\item A description of the system’s final design in terms of UML diagrams (i.e., an updated summary of L2 and the diagram(s) from L3).
\item An evaluation of the achieved results together with problems encountered during the project
(solved ones as well as still open problems).
\item A summary of the project's progress until finalization (see Progress reports):
\begin{itemize}
\item schedule compliance, i.e. how well the project followed the current schedule over time;
\item resources spent per week; and
\item product metrics, like the size or quality of work products. Quality metrics include:
\begin{itemize}
\item Average number of methods per type
\item Average number of fields per type
\item Average lines of code per method
\item Max number of methods per type
\item Max number of fields per type
\item Max lines of code per method
\item Average Cyclomatic Complexity
\item Max Cyclomatic Complexity
\item Comments ratio (the ratio of comments to the number of lines of code)
\item Abstractedness (the ratio of the number of abstract types (abstract classes and interfaces) in the target elements to the total number of types in the target elements)
\end{itemize}
\item A brief discussion about \emph{why} we ask for these particular metrics.
\end{itemize}
\end{itemize}

\subsection{TaPE: Team and Peer Evaluations}
As you submit L4, you are also expected to submit team and peer evaluations (google form, linked from Its learning). Submit one TaPE for each team member, including yourself. These TaPEs will be used to adjust the grades individually based on your performance in the group and course.

\end{document}