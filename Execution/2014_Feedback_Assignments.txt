Assignment 1
====================

Team TAJMS
----------
Bra genomgång av de olika delsystemen som ni skall göra.

Kopplingen mellan Arkitekturen och domänmodellen är otydlig. Vilken kommer ni använda som grund för er implementation?

Jag ser ingen tydlig koppling mellan er Arkitektur eller er domänmodell och er WBS. De komponenter som ni identifierat bör (om de skall implementeras) finnas med som entiteter i WBS:en.

Detta gör det också svårt att bygga trovärdighet för era tidsestimat. Hur motiverar ni timmarna?

Prelimimärt betyg:
 L1.A: 2 (Primärt för att koppling mellan diagrammen saknas)
 L1.W: 2 (Primärt för att arkitekturen inte återspeglas i WBS:en)


Team Quagga
----------
Lägg med teamnamnet på förstasidan framöver; det gör livet aningen enklare för mig.

Tydlig och fin koppling mellan Arkitekturen och domänmodel.
Tyvärr har ni valt ett begränsat verktyg för er WBS. Jag säger inget mer om det nu, men ni märker att det t.ex. är svårt att redovisa nedlagd tid, eller att räkna ut hur lång tid en uppgift faktiskt kommer ta att slutföra när ni väl påbörjat den.

Under 1.2 detailed design så borde man se era respektive komponenter i stället för en klumpsumma på 80h för "Elaborate UML".

Preliminärt betyg:
 L1.A: 4 (Även om kopplingen till erkända spelmotorarkitekturer kunde vara bättre)
 L1.W: 2 (Primärt för att arkitekturen inte återspeglas i WBS:en)



Team Fisk
----------
Om ni skriver ihop en systembeskrivning liknande den som finns för Pacman och TwitterNetHack så kan jag erbjuda den till studenter nästa gång kursen ges.

Otydlig koppling till "erkända spelmotorarkitekturer". Den finns där, men man måste leta efter den och ni diskuterar den inte.

Klar och tydlig koppling till WBS:en.

Preliminärt betyg:
 L1.A: 3
 L1.W: 4



Team Unity
----------
Jag har redan antytt det, men er WBS är väldigt rörig. Den har dessutom en dålig koppling till er arkitektur.

Arkitekturen å sin sida följer fint den av kursboken föreskrivna. Jag kunde önska lite bättre motiveringar för era beslut.

Preliminärt betyg:
 L1.A: 3
 L1.W: 1



Team GetName()
----------
Ok arkitektur, även om jag fick skruva ner kontrasten på min skärm för att inte få huvudvärk. :-)

Se mina kommentarer om WBS:en i veckorapporten (Week 5), framför allt om hur ni räknar er tid.

Det finns i stort sett ingen koppling mellan er arkitektur och er WBS. Detta måste ni också åtgärda.

Preliminärt betyg:
 L1.A: 3
 L1.W: 1 (Primärt för att arkitekturen inte återspeglas i WBS:en, och för att tidsuppskattningarna är imprecist uppdelade)



Assignment 2
====================

Team TAJMS 2014-02-17
----------
Revisit A01
 L1.A: 4
 L1.W: 4 Kan koppla WBS:en ännu hårdare mot arkitekturen. Om jag, t.ex. vill veta hur lång tid som "Ghosts" tar att designa, implementera, och testa, samt hur mycket av detta som redan är gjort, så är det svårt att se. 

Varför är det AI som har GhostStruct och inte Ghost? Och vore det inte mer naturligt att GameManager har koppling till Ghost, som sedan använder sig av AI (eller något annat om man så skulle vilja; t.ex. en nätverksspelare som spelar spökena)?

Ni skulle vinna på en abstract factory, eller ett strategy pattern i er ScreenManager. Även en Factory som sköter all inladdning (T.ex. MapLoader är ju ett embryo till något sådant).

I övrigt; ser bra ut.

 L2.DvsA: 4
 L2.D: 3
 L2.PvsG: 4
 L2.P: 4


Team GetName()
----------
Revisit A01
 L1.A: 4
 L1.W: 4

Ser fint ut. Gillar att ni har en sektion där ni beskriver vilka design patterns som ni använder.

 L2.DvsA: 4
 L2.D: 4
 L2.PvsG: 4
 L2.P: 4

Team Fisk
----------
Revisit A01
 L1.A: 4
 L1.W: 5

 L2.DvsA: 4
 L2.D: 2 (Ser inget bevis på bruk av design patterns; komplettera gärna detta via mail)
 L2.PvsG: 4
 L2.P: 5


Team Unity
----------
Revisit A01
 L1.A: 4
 L1.W: 4


 L2.DvsA: 4
 L2.D: 2 (Ser inget bevis på bruk av design patterns; komplettera gärna detta via mail)
 L2.PvsG: 4
 L2.P: 4


Team Quagga
----------
Det kan synas vara en onödig detalj, men ni sparar mig fem minuter om ni inkluderar team-namnet och deltagarna på första sidan av er rapport.

Revisit A01
 L1.A: 4
 L1.W: 3

Ni har vänt på era aggregat-pilar. Jag läser era diagram som att t.ex. "World består av Game", "GameObject består av Game", "Graphics består av Engine", osv.

Hur kommunicerar Game-komponenten med Engine?

Plus för att ni diskuterar era design patterns.

 L2.DvsA: 4
 L2.D: 2
 L2.PvsG: 3
 L2.P: 3



Assignment 3
====================

Team Fisk
----------
ok

L3.P: 5


Team GetName()
----------
ok

L3.P: 4


Team Unity
----------
ok

L3.P: 4


Team Quagga
----------
Jag måste tyvärr vara lite syntaxpolis. Överföringen mellan två tillstånd skrivs utan hakparenterser. Alltså skrivs t.ex. i ert första diagram överföringen mellan menuscreen och gamescreen helt enkelt som start game. Hakparenteserna betecknar ett vilkor från ett pseudo-state, så i ert andra diagram så räcker det t.ex. med att skriva [FrightenedTimer==done]. Från ett pseudo-state så är den enda andra överföringen (om huvudvilkoret inte passar) ett [else]. Ni kan alltså inte använda det för att införa två vilkor (t.ex. [FrightenedTimer==done] och [killed=true]). Killed är en överföring i sig själv mellan Frightened och Standing in House.

I ert tredje diagram så har ni ytterligare lyft ut all logik ur tillståndet Moves till olika pseudo-states. Moving, Processing Input, Standing still, och Turning vore bättre tillstånd, med lämpliga bågar däremellan.

L3.P: 3


Team TAJMS
----------
Ledsen att jag inte hann kika på er leverans i förtid.

Sid 20, Activity diagrams.
Ni har vänt litegrand på tillståndsdiagrammet; I bubblorna beskriver ni övergångarna mellan tillstånd och vilka "actions" som skall göras, i stället för tvärtom. En nod i ett tillståndsdiagram skall vara ett tillstånd; ett stadium där programmet kör på med samma sak ända tills något händer som gör att man slits ur det tillståndet och hamnar i ett nytt tillstånd. Detta är i det enklaste fallet. Man *kan* sedan köra saker både i tillstånden och i samband med händelser, men det är inte dessa actions som definierar själva händelsen utan är en konsekvens av att händelsen har inträffat.

 

Exempel:

 Nod A: Har ingen glass

 Båge AB: Får glass

 Nod B: "äter glass/tuggar()"

 Båge BC: "brainfreeze/drop_ice_cream()"

 Nod C: huvudvärk

 Båge CA: (kan faktiskt vara utan beskrivning för det ger sig av sammanhanget att om jag inte längre har huvudvärk så återgår jag till ett annat tillstånd)

 Båge BD: "spiller/svär()"

 Nod D: kladdig/torkar()"

 Båge DA: (pss som CA)

 
... Ser ni skilnaden mot era diagram?

L3.P: 3 (Komplettera gärna för att få upp denna ett snäpp eller två)
L4.S: 4

Assignment 4
====================

Team TAJMS
----------
Se A03


Team Unity
----------
ok
L4.S: 2


Team GetName()
----------
Figuren sid. 23: Snygg och överskådlig. Däremot borde man kunna se effekterna av detta (och av diskussionen på sidan 22) i grafen på sidan 24. Kanske en mer detaljerad skala på y-axeln (med steg på, säg, 50h) hade visat detta tydligare.

Jag ger er ett plus för att ni använder er av två externa källor. Det är lite i det minsta laget, men man måste ju börja någonstans.

L4.S: 5


Team Fisk
----------
Revisit L02:
 L2.D: 5

PlayerSystem::UpdateEntity -- Jag hade uppskattat ett lite mer ingående resonemang om _hur_ ni skulle ha förändrat denna metod. Vill jag vara elak så kan jag hävda att ni har misslyckats med att implementera ett entity-component system, om det innebär att ni har skapat en kritiskt nålsöga (PlayerSystem::UpdateEntity) som all spellogik går igenom. Med andra ord så är det bara en person som kan jobba med spellogik samtidigt (Detta syns också ganska tydligt på commit-historiken (fördelen med att Github bara tillåter OSS-projekt; tror jag skall kräva tillgång till GIT-repon nästa år :-), där det är primärt två personer som jobbat med PlayerSystem tills dess committen "fulhack" magiskt löser alla problem ).

${PROJECT_ROOT_URI}/graphs/contributors berättar även att det finns en person som har committat betydligt mindre än de andra. Är det något ni vill berätta för mig?

L4.S: 4


Team Quagga
----------
ok
L4.S: 2

Revisit L02:
  L2.D: 3

