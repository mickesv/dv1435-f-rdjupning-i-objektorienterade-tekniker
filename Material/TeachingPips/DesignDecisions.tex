\documentclass[times,10pt,onecolumn]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage[utf8]{inputenc}
%\usepackage[swedish]{babel}
\usepackage{tikz}

\newcommand{\block}[1]{%
%  \vspace{-20pt}%
  \begin{center}%
    \begin{tikzpicture}%
      \node[rectangle, draw=blue!30, top color=blue!15, bottom color=blue!5, rounded corners=5pt, inner xsep=5pt, inner ysep=6pt, outer ysep=10pt]{
        \noindent\begin{minipage}{\linewidth}#1\end{minipage}};%
    \end{tikzpicture}%
  \end{center}%
%  \vspace{-20pt}%
}

\def\hri #1{\par\vspace{5pt}\noindent {\bf #1.}}
\def\hriblock #1{\par\noindent {\bf #1.}}
\def\mp #1{\marginpar{\vspace{7pt}#1}}



\title{Design Decisions}
\author{Mikael Svahnberg\\
Blekinge Institute of Technology\\
SE-371 79 Karlskrona SWEDEN\\
Mikael.Svahnberg@bth.se}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\begin{abstract}
This Teaching Pip presents a brief discussion about different design decisions.
\end{abstract}

\section{Introduction}
\label{S:Introduction}
If you look into a textbook on software architectures, you will often see an answer to the question ``what is a software architecture?'' roughly on the lines of ``elements, form, and rationale''. The same applies on lower levels; i.e. an object-oriented design can be considered to consist of the classes, the relations between classes, and motivations for these. In this teaching pip I would like to focus a bit on the last bit, since you already know how to model and implement classes and their relations.

Decisions. The mere name sounds boring, especially to software engineers that can't wait to sink their teeth into something real, something concrete, \emph{code}! However, I would like to argue that it is the decisions that actually constitute a software design or a software architecture; the classes and relations are mere implementations of the decisions.

\begin{quote}
``Would you tell me, please, which way I ought to go from here?''\\
``That depends a good deal on where you want to get to.''\\
``I don't much care where ---''\\
``Then it doesn't matter which way you go.''\\
\emph{-- Lewis Carroll, Alice in Wonderland}
\end{quote}

Having said this, I do not mean that you should adopt a cleanroom-like approach to design, where you first try to identify and take all decisions before you start designing. Most of the time you decide and design hand in hand. Some decisions, however, are worth taking a closer look at before you dig deeper into your design, since they strongly influence what you can and what you ought to do in your subsequent activities.

Let's have a look.

\section{Levels of Design Decisions}
\label{S:Levels}
Figure \ref{F:DesignDecisions} lists a number of statements reported under the heading ``design decisions'' in a design assignment report taken (about a Pacman game) in a software design class at Blekinge Institute of Technology (Sorry, they are all in Swedish. Someday I may get around to translating them, but for now, Google Translate is your best friend).

\begin{figure}[bth]  
\block{
\begin{scriptsize}
\begin{itemize}
\item Vi kommer att använda morphanimation för att animera t.ex. att Pacman oöppnar och stänger munnen, ler, rör på sig osv. Vi väljer att inte använda skelettanimation eftersom det skulle tvinga oss att skriva en ny loader för meshes och för att vi anser att Pacman är en för enkel figur för att skelettanimeras.
\item Vi kommer att programmera med utgångspunkt att spelet ska spelas med mus och tangentbord. Detta eftersom vi har programmerat och tolkat input från dessa verktyg förut.
\item Vi har beslutat att alla kollisioner ska skötas med axis-aligned bounding boxar. Detta för att inte fylla ut kollisionshanteraren med onödiga algoritmer som bara används i olika specialfall. Alla kollisioner kommer att hanteras likadant.
\item Vi har beslutat att inte använda något quadträd för att snabba upp kollisions-kollarna. 
\item Vi har beslutat att inte använda någon typ av culling som vi själva implementerar.
\item Vi kommer att ha stöd för 3D-ljud.
\item Vi har beslutat att skriva ut en del text i debug-mode för att kunna se hur mycket prestanda spelet slukar.
\item Vi har beslutat att även rita ut bounding boxarna på något lämpligt sätt i debug-mode. Detta
för att se hur boxarna förhåller sig till objekten de ska omsluta.
\item Vi har bett Mattias Dahlqvist som går på Technical Artist programmet att hjälpa oss modelera
Pacman, spöken och godis. Detta eftersom det förmodligen tar honom fem minuter att
modelera något som skulle ta oss flera timmar att göra.
\item Det kommer att finnas en heads-up display i spelet som visar spelaren hur mycket poäng hen
har, hur många liv Pacman har kvar osv. Spelet är ju ganska värdelöst utan något mål eller någon möjlighet att se hur bra man ligger till.
\item Vi har beslutat att spökenas AI ska vara olika sorters PathFinding som är olika smarta. Exempelvis kan ett spöke g4öra vägval varje gång det krockar med en vägg och göra valet baserat på var Pacman är just då spöket krockar med väggen. Ett smartare AI skulle kunna göra valen oftare än då det krockar med en vägg, exempelvis efter en viss deltatid eller vid olika waypoints som vi programmerat in i kartan.
\item Vi har börjat använda OpenProj för att rita Gantt-schema och WBS-diagram. Det är lite bökigt men vi tror att vi i slutändan kommer få tillbaks tiden eftersom det har fler Gantt och WBS funktioner än Excel som vi använde från början. Vi får även PERT-chart och resurshantering och liknande på köpet, vi kommer alltså att ha en väldigt detaljerad planering av projektet.
\end{itemize}
\end{scriptsize}
}
\caption{Examples of Design Decisions}
\label{F:DesignDecisions}
\end{figure}

What may we learn from these decisions. Probably a lot. Specifically, what I am after in this pip is that \emph{design decisions come in many forms and shapes}!

\newpage
\subsection{Goal Decisions}
Consider the following decision:
\block{
We want the mood to be a little more scary than the original. More spooky sounds and environment. We think that the mood is a little too happy in the original Pacman. It does contain ghosts, and ghosts are scary, therefore it should be a scarier mood.

[\ldots]

What follows is a short extension to the high level concept already laid out in the project document:
{\it ``...Pacman is frightened. The world abruptly turned dark and scary. His visions doesn't extend the boundaries of the walls anymore and light now affects the visibility of the ghosts. Pac-mans food is the only source of light around; so, as he advances, the only safe thing he knows goes away. When ghosts are hidden in the darkness, only their eyes appear to the distraught hero.''}
}

How will this decision influence the software design? Will we be able to implement this in a specific place in the system? A specific class? A specific relation between two classes? Probably not. On the other hand we cannot say that it will not influence the software design. For example, we need to connect the light sources to the food items, and we need to connect the action of eating food to removing light sources. However, the majority of this goal will \emph{not} influence the software design very much. It will have a large impact on the graphics design, and the overall game design, but not so much on the software design.

Let us classify this decision as a decision on the \emph{goal} of the system.

\subsection{Architectural Decisions}
Consider instead the following decision:
\block{
We intend to use morphanimation to animate e.g. that Pacman opens and closes the mouth, smiles, moves, etv. We choose not to use skeletal animation since that would force us to write a new loader for meshes and since we consider Pacman to be too simple a figure for skeletal animation.

[\ldots]

We have decided to not use a quad tree to speed up collission detection.
}

Interstingly enough both these decisions are phrased in the negative, i.e. what they have decided \emph{not} to do. But more importantly, we see that these decisions will actually influence the software architecture of the system. Each decision clearly influences not only single classes to the system, but whole components (granted, some components may be smaller than others).

\hri{Learn More I} In the course ``Software Architecture and Quality'' at Blekinge Institute of Technology, the concept of architecture level decisions are explored in much more detail. Among other things, this includes introducing a full methodology to go from software requirements, identify which architecture decisions that are relevant to take, support for how to take them and how to support different quality requirements, and all the way to implement the architectural decisions in a software architecture.

\hri{Learn More II} Both decisions above replace one way of implementing a particular piece of functionality with another. Thus, the functionality actually stays the same and it would be possible to replace the design decisions (and the components) at a later stage. In software engineering terms, the decisions introduce a \emph{variation point}. For more information about software variability, see e.g. (and I am boosting my ego here) Svahnberg et al. 2005 \cite{Svahnberg:2005c}.

\subsection{Quality Trade Off Decisions}
Trade off decisions may appear at any level and for any design element, but the decisions that are most notable are the ones that deal with the quality attributes of the software system. For example, ``time to develop'' vs ``stability'', or ``performance'' vs ``maintainability''. One may see these decisions as special cases of the architectural decisions, but they occur so frequently that they are worth mentioning in their own right.

\subsection{Low-Level Design Decisions}
\block{
For the ghosts we have chosen to use the strategy pattern. We want every ghost to contain an AI-object which controls the ghosts decisions. Depending on the AI, the algorithm varies slightly allowing the ghosts to behave differently and independently from one another.

[\ldots]

To handle input from the keyboard the observer pattern is used, every gameobject and the menu is a subclass to a listener so they can all become subscribers and get keystrokes. these keystrokes will be handled appropriately be the different subclasses.

[\ldots]

We have decided that the AI of the ghosts shall be different PathFinding algorithms with different levels of intelligence.\footnote{This decision comes from another student group than the first.}
}

\emph{``Finally!''}, says the software engineer, \emph{``Now we are discussing {\bf real} decisions, those that have to do with coding''}.

When you are working with detailed design (and assuming that your architecture level design decisions are already taken), these are the types of decisions that you will fill your day with. Each decision such as the ones above results in a set of classes, a set of interactions between classes, or a specific implementation of methods in a class.

During your detailed design, you will decide on the use of any of a number of \emph{design patterns}. Sometimes the selection of a particular design pattern is the decision in itself (as wth the observer pattern above), and sometimes your decision will best be implemented with a particular design pattern (for example, the strategy pattern is a good candidate for the last decision above).

\hri{Learn More} See the Teaching Pip on Design Patterns.

\subsection{Summary}
We thus have four levels of decisions in a small software engineering project. At least. There are other dimensions of decisions as well, for example ``what to build vs how to build it'' decisions that impacts the workflow but not the software artefacts.

To summarise, the levels discussed are:

\begin{itemize}
\item Goal Decisions
\item Architectural Decisions
\item Quality Trade Off Decisions
\item Low-Level Design Decisions
\end{itemize}

\subsection{Traceability}
\begin{quote}
[\ldots]\\
She swallowed the spider to catch the fly;\\
I don't know why she swallowed a fly -- Perhaps she'll die!\\
\end{quote}

The \emph{reason} why I like documented decisions that much is because they provide \emph{traceability}. I can look at a piece of code and then read the documented decisions in order to figure out why the code is implemented as it is, why the classes interact as they do, and why the particular design patterns were chosen. In an ideal world (I once had a math teacher who defined an ideal world as an infinitely large abacus and a sufficient amount of time; then he could do anything that we did on our pesky computers), the decisions thus create an unbroken chain from requirements down to code elements, and back up again. If I need to introduce a change, I can understand how to do that change by looking at the decisions already taken.

In a less than ideal world (such as the one we are living in), to create and maintain this unbroken link of design decisions is too expensive (and to boring) for any sane software developer to bother. In these situations, as the unadulterated academic I am, I fall back to my other reason: It is good for you.

It is good for you to at least once in your life have considered what you do, to the level that you explicitly take decisions about every aspect of your code. To identify \emph{why} you do as you do, and to consider the alternatives. Once you graduate, you will rarely have time to do this, so by then you'd better know your reasons for why you favour certain design patterns or algorithms in certain situations. By then, you should also have learnt to identify \emph{when} an explicit decision is necessary, and when it is sufficient to simply hack it.

\hri{One more thing\ldots} The quote above points to one important aspect that must not be forgotten. Your design decisions may very well shape your software system, but they are only as good or bad as the original motivation was. Why \emph{did} she swallow the fly in the first case?

\bibliographystyle{abbrv}
\bibliography{/Users/msv/Documents/Research/references/references.bib}

\end{document}
